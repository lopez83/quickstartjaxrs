package com.lopez83.qstart.jaxrs;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloWorld {

    @GET
    @Path("/echo/{input}")
    @Produces("text/plain")
    public String ping(@PathParam("input") String input) {
        return input;
    }

    @GET
    @Path("/printjson/{input}")
    @Produces("application/json")
    public Response printJson(@PathParam("input") String input) {
        //return input;
    	JsonBean bean = new JsonBean();
    	bean.setVal1(input);
    	bean.setVal2("hardcoded");
        return Response.ok().entity(bean).build();
    }
    
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/jsonBean")
    public Response modifyJson(JsonBean input) {
        input.setVal2(input.getVal1());
        return Response.ok().entity(input).build();
    }
}

